pom
==
<dependency>
   <groupId>org.ehcache</groupId>
   <artifactId>ehcache</artifactId>
   <version>3.2.0</version>
</dependency>


application
==
@Bean
CacheManager cacheManager() {
   return CacheManagerBuilder.newCacheManagerBuilder()
           .withCache("preConfigured", CacheConfigurationBuilder.newCacheConfigurationBuilder(
                   String.class,
                   Long.class,
                   ResourcePoolsBuilder.heap(100)
                   ).build()
           ).build(true);
}

@Bean
Cache<String, Long> meetingSessionIdCache() {
   return cacheManager().createCache(
           "meetingSessionIdCache",
           CacheConfigurationBuilder.newCacheConfigurationBuilder(
                   String.class,
                   Long.class,
                   ResourcePoolsBuilder.heap(100)
           ).build());
}


usage
==
//to get
Long meetingSessionId = meetingSessionIdCache.get(streamName);
//to set
meetingSessionIdCache.put(streamName, meetingSessionId);

